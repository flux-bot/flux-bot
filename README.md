Numerical model for estimating water FLUXes Based On Temperatures-FLUX-BOT


Model description

FLUX-BOT is a numerical code written in MATLAB that calculates vertical water fluxes in saturated sediments based on the inversion of measured temperature time series.  It applies a centered Crank-Nicolson implicit finite difference scheme to solve the one-dimensional heat advection-conduction equation. FLUX-BOT is inspired by Laphamâ€™s pioneering work (Lapham, 1989), who applied a numerical solution to the heat flow heat advection-conduction equation in a forward way.
Version 0.0.1


Model use

Add  the folder containing the FLUX-BOT files to the MATLAB search path. The fluxbot folder contains all data, scripts and functions needed to run fluxbot and the provides verification examples. 